package reader.resources;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Robertas
 */
@Path("/")
public class Memory {
    /**
     * Finds how much memory in total there is
     *
     * @param output
     * @return
     */
    public static String getTotalMemory(String output)
    {
        Pattern p;
        Matcher m;

        String totalMemory = "";

        // extract how much memory does device have
        p = Pattern.compile("MemTotal:\\s+(\\d+)");
        m = p.matcher(output);
        if(m.find()) {
            totalMemory = m.group(1);
        }

        return totalMemory;
    }


    /**
     * Finds how much free memory there is
     *
     * @param output
     * @return int
     */
    public static String getFreeMemory(String output)
    {
        Pattern p;
        Matcher m;

        String freeMemory = "";

        // extract how much free memory there is
        p = Pattern.compile("MemFree:\\s+(\\d+)");
        m = p.matcher(output);

        if(m.find()) {
            freeMemory = m.group(1);
        }

        return freeMemory;
    }

    @GET
    @Path("/json")
    @Produces({MediaType.APPLICATION_JSON})
    public Map<String, String> getMessage() {
        String command = "cat /proc/meminfo";

        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine())!= null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String timestamp = Long.toString(System.currentTimeMillis() / 1000L);

        // passing data as JSON, there is dedicated third party library for this
        // but not used because it would be too heavy for such a task
        Map<String,String> result = new HashMap<String, String>();

        result.put("total_memory", getTotalMemory(output.toString()));
        result.put("free_memory", getFreeMemory(output.toString()));
        result.put("timestamp", timestamp);

        return result;
    }
}
