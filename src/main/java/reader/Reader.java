package reader;

import com.sun.jersey.api.container.grizzly2.GrizzlyWebContainerFactory;
import org.glassfish.grizzly.http.server.HttpServer;

import java.util.HashMap;
import java.util.Map;

public class Reader {

    public Reader(int port) {
        final Map<String, String> init_params = new HashMap<String, String>();
        init_params.put("com.sun.jersey.config.property.packages", Config.JAXRS_RESOURCES);
        init_params.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        try {
            String baseurl = Config.getBaseURL(port);
            HttpServer server = GrizzlyWebContainerFactory.create(baseurl, init_params);
            System.out.println("Reader server started: " + baseurl);
            System.in.read();
            System.out.print("Stop server...");
            server.stop();
            System.out.println("done");
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Reader(Config.PORT);
    }
}