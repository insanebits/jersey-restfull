package client;

import common.IServiceConnector;
import common.UsageServiceConnector;

import javax.swing.*;

/**
 * Created by Robertas Jasmontas on 14.9.29.
 */
public class Client {
    public static void main(String [] args)
    {
        JFrame window = new Window("Client");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

}
