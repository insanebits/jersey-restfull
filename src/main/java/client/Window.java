package client;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by user on 9/30/2014.
 */
public class Window extends JFrame {
    private DefaultTableModel tableModel;
    private JTable table;
    private UsageTableUpdater tableUpdater;
    private ScheduledExecutorService executor;

    public Window(String title) throws HeadlessException {
        super(title);

        addComponents();

        // (Need to make variable final *if* it is a local (method) variable.)
        executor = Executors.newSingleThreadScheduledExecutor();
        tableUpdater = new UsageTableUpdater(table, executor);

        try {
            tableUpdater.call();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void addComponents()
    {
        setLayout(new BorderLayout());

        JLabel label = new JLabel("Your computer resource usage", SwingConstants.CENTER);
        add(label, BorderLayout.CENTER);

        Object[][] data = {};

        String[] columns = {"Memory used","Free memory MB", "Time"};;

        tableModel = new DefaultTableModel(data, columns);

        table = new JTable(tableModel);
        table.setFillsViewportHeight(true);

        JScrollPane panel = new JScrollPane(table);

        panel.setVisible(true);


        add(panel, BorderLayout.CENTER);
    }
}
