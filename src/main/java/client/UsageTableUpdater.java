package client;

import common.IServiceConnector;
import common.UsageServiceConnector;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by insane on 14.9.29.
 */
public class UsageTableUpdater implements Callable {
    private DefaultTableModel model;
    private JTable table;
    private ScheduledExecutorService executor;

    public UsageTableUpdater(JTable dtm, ScheduledExecutorService svc)
    {
        table = dtm;
        model = (DefaultTableModel)dtm.getModel();
        executor = svc;
    }

    @Override
    public Object call() throws Exception {
        try {
            System.out.println("Retrieving usage data");

            IServiceConnector connector = new UsageServiceConnector();

            if(connector.connect())
            {
                String data = connector.receive();

                System.out.println("Received data: " + data);

                JSONTokener tokener = new JSONTokener(data);
                JSONArray root = new JSONArray(tokener);

                model.getDataVector().clear();

                for(int i = 0; i < root.length(); i++)
                {
                    JSONObject row = root.getJSONObject(i);

                    int free = Integer.valueOf(row.getString("free_memory"));
                    int total = Integer.valueOf(row.getString("total_memory"));

                    float percent = 100.0F - (((float)free) / total) * 100.0F;
                    int memory_used = (total - free) / 1024;

                    Long timestamp = Long.parseLong(row.getString("timestamp")) * 1000L;
                    String date = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date(timestamp));

                    String[] table_row = new String[]{
                            Float.toString(percent) + "%",
                            memory_used + " MB",
                            date
                    };
                    model.addRow(table_row);
                }

                table.invalidate();


            } else {
                System.out.println("Failed to connect to Usage Service");
            }
        }finally {
            // Schedule same task to run again (even if processing fails).
            executor.schedule(this, 1, TimeUnit.SECONDS);
        }

        return null;
    }
}
