package storage;

import com.sun.jersey.api.container.grizzly2.GrizzlyWebContainerFactory;
import org.glassfish.grizzly.http.server.HttpServer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Storage {

    public Storage(int port) {

        final Map<String, String> init_params = new HashMap<String, String>();
        init_params.put("com.sun.jersey.config.property.packages", Config.JAXRS_RESOURCES);
        init_params.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        // launch process that will periodically feed data from reader to storage
        startDataFeeder();

        System.out.print("Starting Grizzly...");
        try {
            String baseurl = Config.getBaseURL(port);
            HttpServer server = GrizzlyWebContainerFactory.create(baseurl, init_params);
            System.out.println("Storage server started: " + baseurl);
            System.in.read();
            System.out.print("Stop server...");
            server.stop();
            System.out.println("done");
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Storage(Config.PORT);
    }

    public static void startDataFeeder()
    {
        // (Need to make variable final *if* it is a local (method) variable.)
        final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        Callable<Void> dataReceiver = new MemoryUsageReceiver(DataStorage.getInstance(), scheduler);

        try {
            dataReceiver.call();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}