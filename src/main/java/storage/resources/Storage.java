package storage.resources;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import storage.DataStorage;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * @author Robertas Jašmontas
 */
@Path("/")
public class Storage {

    @GET
    @Path("/retrieve")
    @Produces({MediaType.APPLICATION_JSON})
    public JSONArray retrieve()
    {
        DataStorage storage = DataStorage.getInstance();

        JSONArray result = new JSONArray();

        if(storage != null)
        {
            for (Object item : storage.getArray())
            {
                JSONObject obj = null;

                try {
                    obj = new JSONObject(item.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                result.put(obj);
            }
        }

        return result;
    }
}
