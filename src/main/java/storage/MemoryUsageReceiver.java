package storage;

import common.IServiceConnector;
import common.UsageReaderConnector;
import storage.DataStorage;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by insane on 14.9.29.
 */
public class MemoryUsageReceiver implements Callable {
    private ScheduledExecutorService executor;
    private String address = "//localhost/UsageReader";

    public MemoryUsageReceiver(DataStorage ds, ScheduledExecutorService svc)
    {
        executor = svc;
    }

    @Override
    public Object call() throws Exception {
        try {
            System.out.println("Updating storage data");

            IServiceConnector connector = new UsageReaderConnector();

            if(connector.connect())
            {
                String data = connector.receive();

                System.out.println("Received data: " + data);

                DataStorage.getInstance().insert(data);
                DataStorage.getInstance().preserve();
            } else {
                System.out.println("Failed to connect to " + address);
            }
        }finally {
            // Schedule same task to run again (even if processing fails).
            executor.schedule(this, 5, TimeUnit.SECONDS);
        }

        return null;
    }
}
