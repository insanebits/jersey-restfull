package storage;

/**
 * The configuration
 *
 * @author Dennis Boldt
 *
 */
public class Config {

    public static final String HOSTNAME = "localhost";
    public static final Integer PORT = 9998;
    public static final String JAXRS_RESOURCES = "storage";

    public static String getBaseURL(Integer port) {
        if(port == null) {
            port = Config.PORT;
        }
        StringBuilder sb = new StringBuilder("http://");
        sb.append(Config.HOSTNAME).append(":").append(String.valueOf(port)).append("/");
        return sb.toString();
    }
}
