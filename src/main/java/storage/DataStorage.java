package storage;

import java.io.*;
import java.util.LinkedList;

/**
 * Created by insane on 14.9.29.
 */
public class DataStorage implements java.io.Serializable {
    public static final String DEFAULT_PATH = "storage.dat";
    private LinkedList<String> data;
    private transient Object lock = new Object();
    private Integer size = 10;


    protected static DataStorage instance = null;

    public DataStorage()
    {
        data = new LinkedList<String>();
    }

    public void insert(String node)
    {
        //synchronized (lock) {
            // add new element to the start of list
            data.addFirst(node);

            // limit size to given
            if(data.size() > size)
            {
                // remove last element from the list
                data.removeLast();
            }
        //}
    }

    public Object[] getArray()
    {
        return data.toArray();
    }

    public static DataStorage getInstance()
    {
        if(instance == null)
        {
            ObjectInputStream ois = null;
            Boolean loaded = false;

            try {
                ois = new ObjectInputStream(new FileInputStream(DEFAULT_PATH));
                instance = (DataStorage) ois.readObject();
                loaded = true;
            } catch (IOException e) {
                System.out.println("Storage file not found");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            if(!loaded)
            {
                instance = new DataStorage();
            }
        }

        return instance;
    }

    public Boolean preserve()
    {
        Boolean status = false;
        // if instance not initialized do nothing
        if(instance != null)
        {
            try {
                FileOutputStream fos = new FileOutputStream(DEFAULT_PATH);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(instance);
                oos.close();
                fos.close();

                status = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return status;
    }
}
