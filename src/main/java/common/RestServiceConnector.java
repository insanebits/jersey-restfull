package common;

import org.codehaus.jettison.json.JSONObject;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by insane on 14.10.22.
 */
public class RestServiceConnector implements IServiceConnector {
    URL address;

    Service service;

    protected RestServiceConnector(String address)
    {
        try {
            this.address = new URL(address);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    public String receive() {

        InputStream is = null;
        StringBuilder sb = null;

        try {

            is = this.address.openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //is.close();
        }

        return sb.toString();
    }
}
