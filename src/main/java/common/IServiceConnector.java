package common;

/**
 * Created by Robertas Jasmontas on 14.9.29.
 */
public interface IServiceConnector {
    /**
     * Connect to Reader
     *
     * @return bool - if connection was successful
     */
    public boolean connect();

    /**
     * Receive data
     *
     * @return string with data
     */
    public String receive();
}
